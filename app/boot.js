/**
 * Created by iashind on 08.04.15.
 */
var winston = require('winston');
try {
    var config = require('./config');
} catch (err) {
    config = {
        mySql: {}
    };
}

var services = {};
services.logger = new (winston.Logger)({
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            dirname: 'logs',
            filename: 'app-info.log',
            level: 'info'
        }),
        new (winston.transports.File)({
            name: 'error-file',
            dirname: 'logs',
            filename: 'app-error.log',
            level: 'error'
        })
    ]
});
services.db = require('./lib/db')(config.db, services.logger);
services.router = require('./lib/router');
services.auth = require('./lib/auth')(services);
services.parser = require('./lib/parser');
services.mysql = require('./lib/mysql')(config.mySql);
services.tools = require('./lib/tools');
services.socket = require('./lib/sockets');

module.exports = {
    config: config,
    services: services
};