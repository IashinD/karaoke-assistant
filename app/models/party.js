/**
 * Created by iashind on 09.04.15.
 */
var mongoose = require('mongoose');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var _ = require('lodash');

var Schema = new mongoose.Schema({
    admin: {type: mongoose.Schema.Types.ObjectId, ref: 'Admin'},
    start: {type: Number, default: (Date.now())},
    end: {type: Number, default: 0},
    clients: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Client'
        }]
    },
    orders: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Order'
        }]
    }
});

Schema.statics.addClient = async(function (client, partyId) {
    var Party = mongoose.model('Party'),
        party = await(Party.findOne({_id: partyId}).exec());

    party.clients.push(mongoose.Types.ObjectId(client._id));
    party.save();
    return await(Party.findOne({_id: partyId}).exec());
});

Schema.statics.new = function (adminId) {
    var Party = mongoose.model('Party'),
        party = new Party({admin: adminId});

    party.save();
    return party;
};

Schema.statics.end = function (party) {
    party.end = Date.now();
    party.save();
};

mongoose.model('Party', Schema);
