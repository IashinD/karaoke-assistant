/**
 * Created by iashind on 09.04.15.
 */
var mongoose = require('mongoose');
var Schema = new mongoose.Schema({
    songId: {type: Number},
    clientId: {type: mongoose.Schema.Types.ObjectId, ref: 'Client'},
    tone: {type: Number, default: 0},
    speed: {type: Number, default: 0},
    comment: {type: String},
    passed: {type: Boolean, default: false},
    deleted: {type: Boolean, default: false}
});

Schema.statics.new = function (order) {
    order = new this(order);
    order.save();
    return order;
};

mongoose.model('Order', Schema);