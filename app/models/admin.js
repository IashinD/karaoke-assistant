/**
 * Created by iashind on 23.04.15.
 */
var mongoose = require('mongoose');
var Schema = new mongoose.Schema({
    username   : {type: String, trim: true, unique: true},
    password: {type: String},
    socketId: {type: String},
    temp: {type: Boolean, default: true}
});

mongoose.model('Admin', Schema);