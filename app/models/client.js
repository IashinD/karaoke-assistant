/**
 * Created by iashind on 09.04.15.
 */
var mongoose = require('mongoose');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var Schema = new mongoose.Schema({
    userId  : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    partyId : {type: mongoose.Schema.Types.ObjectId, ref: 'Party'},
    amount  : {type: Number},
    orders  : {type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Order'}]},
    isActive: {type: Boolean, default: false},
    isLeft  : {type: Boolean, default: false},
    lastFive: {type: String},
    socketId: {type: String}
});

Schema.statics.link = async(function (data) {
    var Client = mongoose.model('Client'),
        client = await(Client.findOne({_id: data._id}).exec());

    client.userId = data.userId;
    client.save();
    return client.userId;
});

Schema.statics.addOrder = async(function (clientId, order) {
    var client = await(this.findOne({_id: clientId}).exec());

    client.orders.push(order._id);
    client.save();
    return client;
});

Schema.statics.new = async(function (partyId) {
    var Client = mongoose.model('Client'),
        client = new Client({
            partyId: partyId
        }),
        toString = client._id.toString();

    client.lastFive = toString.substr(-5);
    client.save();
    return client;
});

mongoose.model('Client', Schema);