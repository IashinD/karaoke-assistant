/**
 * Created by iashind on 09.04.15.
 */
var mongoose    = require('mongoose'),
    Joi         = require('joi'),
    Q           = require('q'),
    async       = require('asyncawait/async'),
    await       = require('asyncawait/await'),
    Schema      = new mongoose.Schema({
        regDate     : {type: Number, default: (Date.now())},
        lastVisit   : {type: Number, default: (Date.now())},
        username    : {type: String},
        password    : {type: String, require: true},
        email       : {type: String, require: true, unique: true},
        tunes       : {type: Object, default: {}},
        favs        : {type: Array, default: []}
    }),
    JoiSchema = Joi.object().keys({
        lastVisit: Joi.number(),
        username: Joi.string().min(3).max(20),
        password: Joi.string().required(),
        email: Joi.string().email().required()
    });

Schema.statics.validate = function (data) {

    return Joi.validate(data, JoiSchema);
};

Schema.statics.login = function (data) {
    var user = await(this.findOne({email: data.email, password: data.password}).exec());
    if (!user) {
        return null;
    }
    user.lastVisit = Date.now();
    user.save();
    return user;
};

Schema.statics.reg = async(function (data) {
    var user,
        error,
        defer = Q.defer();

    if (error = this.validate(data).error) {
        defer.reject(error);
        return defer.promise;
    }
    user = new this(data);

    user.save(function (err) {
        if (err) {
            return defer.reject(err);
        }
        defer.resolve(user);
    });
    return defer.promise;
});

mongoose.model('User', Schema);