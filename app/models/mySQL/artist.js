/**
 * Created by iashind on 20.04.15.
 */
var Q = require('q');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var squel = require('squel');



function Artist (pool) {
    this.pool = pool;
    pool.getConnection(function (err, connection) {
        if (err) {
            throw new Error(err);
        }
        var sql = "CREATE TABLE IF NOT EXISTS `artist` (`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,`title` VARCHAR(128) NOT NULL DEFAULT 'НЕИЗВЕСТНЫЙ АРТИСТ',PRIMARY KEY (`id`),UNIQUE INDEX `id_UNIQUE` (`id` ASC), UNIQUE INDEX `title_UNIQUE` (`title` ASC)) ENGINE=InnoDB CHARACTER SET=UTF8;";

        connection.query(sql, function (err) {
            if (err) {
                throw new Error(err);
            }
            connection.release();
        });
    });
}

Artist.prototype.populateData = async(function (data){
    var def = Q.defer(),
        _this = this,
        i,
        artists = [],
        artist;

    for (i = 0; i < data.length; i++) {
        artist = data[i][2];
        if (artists.indexOf(artist) === -1) {
            artists.push(artist);
        }
    }
    squel.useFlavour('mysql');
    this.pool.getConnection(function (err, conn) {
        var selectSQL;
        if (err) {
            throw new Error(err);
        }
        selectSQL = squel.select({
            autoQuoteAliasNames: true,
            autoQuoteFieldNames: true,
            autoQuoteTableNames: true
        }).from('artist').distinct().where('title in (' + _this._getWrappedString(artists) + ')').toString();
        conn.query(selectSQL, function (err, rows) {
            var cache = {},
                toInsert,
                insertSQL;

            if (err) {
                throw new Error(err);
            }
            _this._populateTitleWithId(data, rows, cache);
            toInsert = _this._getNotPopulated(data);
            if (!toInsert.length) {
                conn.release();
                return def.resolve(data);
            }
            for (i = 0; i < toInsert.length; i++) {
                toInsert[i] = {
                    title: _this._addSlashes(toInsert[i])
                };
            }
            insertSQL = squel.insert({
                autoQuoteAliasNames: true,
                autoQuoteFieldNames: true,
                autoQuoteTableNames: true
            }).into('artist').setFieldsRows(toInsert).toString();
            conn.query(insertSQL, function (err, rows) {
                if (err) {
                    throw new Error(err);
                }
                conn.query(selectSQL, function (err, rows) {
                    if (err) {
                        throw new Error(err);
                    }
                    _this._populateTitleWithId(data, rows, cache);
                    def.resolve(data);
                    conn.release();
                });
            });
        });
    });
    return def.promise;
});

Artist.prototype._getWrappedString = function (array) {
    var string = '', i;
    for (i = 0; i < array.length; i++) {
        string += '"' + this._addSlashes(array[i]) + '",';
    }
    return string.slice(0, -1);
};

Artist.prototype._addSlashes = function (string) {
    if(typeof string !== 'string') {
        string = String(string);
    }
    return string.replace(/[\\\/'"]/g, function(symbol) {
        return '\\' + symbol;
    })
};

Artist.prototype._populateTitleWithId = function (data, rows, cache) {
    var i, j, title;

    for (i = 0; i < data.length; i++) {
        title = data[i][2];
        if (cache[title]) {
            data[i][2] = cache[title];
            continue;
        }
        for (j = 0; j < rows.length; j++) {
            if (rows[j].title === title) {
                data[i][2] = cache[title] = rows[j].id;
                break;
            }
        }
    }
};

Artist.prototype._getNotPopulated = function (data) {
    var i, result = [];
    for (i = 0; i < data.length; i++) {
        if (typeof data[i][2] !== 'number' && result.indexOf(data[i][2]) === -1) {
            result.push(data[i][2]);
        }
    }
    return result;
};

module.exports = function (pool) {
    return new Artist(pool);
};