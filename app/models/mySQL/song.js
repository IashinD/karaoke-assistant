/**
 * Created by iashind on 20.04.15.
 */
var Q = require('q');

module.exports = function (pool) {

    pool.getConnection(function (err, connection) {
        if (err) {
            throw new Error(err);
        }
        var sql = "CREATE TABLE IF NOT EXISTS `song` (`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,`code` MEDIUMINT NOT NULL,`title` VARCHAR(45) NOT NULL DEFAULT 'БЕЗ НАЗВАНИЯ',`artist` INT NOT NULL,`vocal` TINYINT(1) NULL DEFAULT 0,`quality` TINYINT(1) NULL DEFAULT 0,`text` VARCHAR(45) NULL,`sound` VARCHAR(45) NULL,`createdAt` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,PRIMARY KEY (`id`),UNIQUE INDEX `code_UNIQUE` (`code` ASC))ENGINE=InnoDB CHARACTER SET=UTF8;";
        connection.query(sql, function (err) {
            if (err) {
                throw new Error(err);
            }
            connection.release();
        });
    });

    return {
        pool: pool,
        getList: function (params) {
            var def = Q.defer(),
                query = 'SELECT SQL_CALC_FOUND_ROWS song.id, song.code, song.title, artist.title as artist FROM song RIGHT JOIN artist ON song.artist = artist.id',
                searchString,
                favs;

            function getWhereOrAnd(query) {
                return ~query.indexOf('WHERE') ? ' AND' : ' WHERE';
            }

            if (params.pro === 'true') {
                query += ' WHERE song.code > 7999';
            }
            if (params.searchString) {
                searchString = this.pool.escape(params.searchString).toUpperCase();
                query += getWhereOrAnd(query);
                query += ' (UPPER(song.title) RLIKE ' + searchString + ' OR UPPER(artist.title) RLIKE ' + searchString + ')';
            }
            if (params.nation == 2) { //ToDo: set codes range more accurately
                query += getWhereOrAnd(query);
                query += ' song.code >= 12000 AND song.code < 60000';
            } else if (params.nation == 1) {
                query += getWhereOrAnd(query);
                query += ' (song.code < 12000 OR song.code >= 60000)';
            }
            if (params.favorites === 'true') {
                favs = this.pool.escape(params.favs);
                query += getWhereOrAnd(query);
                query += ' song.id IN (' + favs + ')';
            }
            query += ' ORDER BY artist.title LIMIT ?, 20';

            this.pool.getConnection(function (err, connection) {
                if (err) {
                    throw new Error(err);
                }
                connection.query(query, [(+params.page - 1) * 20], function (err, rows) {
                    if (err) {
                        return def.reject(err);
                    }
                    connection.query('SELECT FOUND_ROWS() as count', function (err, res) {
                        def.resolve({
                            rows: rows,
                            totalCount: res[0].count
                        });
                        connection.release();
                    });
                });
            });
            return def.promise;
        },
        getOne: function (id) {
            var def = Q.defer(),
                query = 'SELECT song.id, song.code, song.title, artist.title as artist FROM song RIGHT JOIN artist ON song.artist = artist.id WHERE song.id = ?';
            this.pool.getConnection(function (err, connection) {
                if (err) {
                    throw new Error(err);
                }
                connection.query(query, [+id], function (err, rows) {
                    if (err) {
                        return def.reject(err);
                    }
                    def.resolve(rows[0]);
                    connection.release();
                });
            });
            return def.promise;
        },
        update: function (data) {
            var def = Q.defer();
            data = this.pool.escape(data);
            this.pool.query('INSERT IGNORE INTO `song` (code, title, artist, vocal, quality) VALUES ' + data, function (err, result) {
                if (err) {
                    throw new Error(err);
                }
                def.resolve(result);
            });
            return def.promise;
        }
    };
};