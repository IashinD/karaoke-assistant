/**
 * Created by iashind on 08.04.15.
 */
module.exports = function (config, logger) {
    var mongoose = require('mongoose');
    var glob = require('glob');

    mongoose.connect(process.env.MONGOLAB_URI || config.uri);
    mongoose.connection.on('error', logger.error);

    glob.sync('../models/**.js', { cwd: __dirname }).forEach(require);

    return mongoose;
};