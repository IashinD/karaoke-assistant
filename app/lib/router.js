/**
 * Created by iashind on 08.04.15.
 */
var _ = require('lodash');
var express = require('express');
var IndexCtrl = require('../controllers/index');
var PartyCtrl = require('../controllers/party');
var ClientCtrl = require('../controllers/client');
var UserCtrl = require('../controllers/user');
var SongsCtrl = require('../controllers/song');
var UploadCtrl = require('../controllers/upload');
var AdminCtrl = require('../controllers/admin');
var router = module.exports = express.Router();

[
    {
        path: '/',
        method: 'get',
        middleware: [IndexCtrl.index]
    },
    {
        path: '/api/party/:admin',
        method: 'get',
        middleware: [AdminCtrl.isAdmin, PartyCtrl.start]
    },
    {
        path: '/api/party/:id',
        method: 'put',
        middleware: [AdminCtrl.isAdmin, PartyCtrl.endParty]
    },
    {
        path: '/api/clients',
        method: 'post',
        middleware: [AdminCtrl.isAdmin, ClientCtrl.addClient]
    },
    {
        path: '/api/clients',
        method: 'get',
        middleware: [AdminCtrl.isAdmin, ClientCtrl.getClients]
    },
    {
        path: '/api/clients/:id',
        method: 'get',
        middleware: [ClientCtrl.isLeft, ClientCtrl.getById]
    },
    {
        path: '/api/clients',
        method: 'put',
        middleware: [ClientCtrl.isLeft, ClientCtrl.updateClient]
    },
    {
        path: '/api/user',
        method: 'post',
        middleware: [UserCtrl.regUser]
    },
    {
        path: '/api/user/:email',
        method: 'post',
        middleware: [UserCtrl.check]
    },
    {
        path: '/api/user/:id',
        method: 'get',
        middleware: [UserCtrl.getUser]
    },
    {
        path: '/api/user/',
        method: 'put',
        middleware: [ClientCtrl.isLeft, UserCtrl.updateUser]
    },
    {
        path: '/api/songs',
        method: 'get',
        middleware: [ClientCtrl.isLeft, SongsCtrl.getList]
    },
    {
        path: '/api/songs/:id',
        method: 'get',
        middleware: [ClientCtrl.isLeft, SongsCtrl.getOne]
    },
    {
        path: '/api/order/',
        method: 'post',
        middleware: [ClientCtrl.isLeft, PartyCtrl.order]
    },
    {
        path: '/api/order/:id',
        method: 'put',
        middleware: [ClientCtrl.isLeft, PartyCtrl.updateOrder]
    },
    {
        path: '/api/manager/order/:id',
        method: 'put',
        middleware: [PartyCtrl.setPassed]
    },
    {
        path: '/api/upload',
        method: 'post',
        middleware: [SongsCtrl.upload]
    },
    {
        path: '/admin/:username',
        method: 'post',
        middleware: [AdminCtrl.login]
    },
    {
        path: '/admin/:username',
        method: 'put',
        middleware: [AdminCtrl.isAdmin, AdminCtrl.update]
    }
]
    .forEach(function (route) {
        router[route.method].apply(router, _.flatten([route.path, route.middleware]));
    });
