/**
 * Created by iashind on 25.05.15.
 */
var async   = require('asyncawait/async'),
    await   = require('asyncawait/await'),
    _       = require('lodash');

function SocketManager() {
    this.queue = [];
}

SocketManager.prototype.getRecipient = async(function (options) {
    if (options.clientId) {
        return await(options.app.services.db.model('Client').findOne({_id: options.clientId}).populate('orders').populate('userId').exec());
    }
    return await(options.app.services.db.model('Admin').findOne({_id: options.adminId}).exec());
});

SocketManager.prototype.getTimeoutDuration = function (options) {
    if (!options.tries) {
        options.tries = 0;
    }
    options.tries++;

    return options.tries < 60 ? 1000 * options.tries : 60000;
};

SocketManager.prototype.pushMessage = async(function (options, logger) {
    options.recipient = await(this.getRecipient(options));
    if (this.queue.indexOf(options.recipient._id.toString()) !== -1) {
        return;
    }

    this.queue.push(options.recipient._id.toString());
    this.sendMessage(options, logger);
});

SocketManager.prototype.sendMessage = async(function (options, logger) {
    var manager = this,
        timeout = this.getTimeoutDuration(options),
        socket;

    if (options.tries) {
        if (options.tries > 100) {
            return logger.error('Tries limit reached. Sending stop.');
        }
        _.assign(options.recipient, await(this.getRecipient(options)));
    }

    socket = options.app.sockets[options.recipient.socketId];
    if (!socket) {
        logger.error('fail: !socket', {
            recipientName: options.recipient.username
        });

        return setTimeout(function () {
            manager.sendMessage(options, logger);
        }, timeout);
    } else {
        try {
            socket.send(JSON.stringify({
                event: options.evt,
                data: options.data
            }), {compress: false});
        } catch (err) {
            logger.error('fail: error', {
                recipientName: options.recipient.username
            });
            logger.error('error: ', err);
            return setTimeout(function () {
                manager.sendMessage(options, logger);
            }, timeout);
        }
    }
    this.clearQueue(options.recipient._id.toString());
});

SocketManager.prototype.clearQueue = function (id) {
    var index = this.queue.indexOf(id);
    this.queue.splice(index, 1);
};

module.exports = new SocketManager();



