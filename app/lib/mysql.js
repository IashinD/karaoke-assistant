/**
 * Created by iashind on 20.04.15.
 */

var mysql = require('mysql');

module.exports = function (config) {
    var pool,
        Song,
        Artist,
        parse;

    if (process.env.CLEARDB_DATABASE_URL) {
        parse = process.env.CLEARDB_DATABASE_URL.match(/\/\/([\w\W]*):([\w\W]*)@([\w\W]*)\/([\w\W]*)\?/i);
        config.user = parse[1];
        config.password = parse[2];
        config.host = parse[3];
        config.database = parse[4];
    }

    pool = mysql.createPool(config);
    Song = require('./../models/mySQL/song')(pool);
    Artist = require('./../models/mySQL/artist')(pool);

    return {
        pool: pool,
        models: {
            Song: Song,
            Artist: Artist
        }
    };
};