/**
 * Created by iashind on 12.05.15.
 */
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var _ = require('lodash');
var actions = module.exports;

actions.assignModel = function (model, obj) {
    var i;
    for (i in obj) {
        if (!obj.hasOwnProperty(i) || i[0] === '_') {
            continue;
        }
        model[i] = obj[i];
    }
};

actions.onlyOwnProperties = function (obj) {
    var onlyOwn = {},
        i;

    for (i in obj) {
        if (obj.hasOwnProperty(i)) {
            console.log(i);
            onlyOwn[i] = obj[i];
        }
    }
    return onlyOwn;
};