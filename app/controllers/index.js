/**
 * Created by iashind on 08.04.15.
 */
module.exports.index = function index(req, res) {
    res.render('index');
};

module.exports.getSocketOptions = function (req, res) {
    res.send({
        protocol: process.env.isHeroku ? 'https' : 'http',
        port: req.app.get('port')
    });
};