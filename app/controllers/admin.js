/**
 * Created by iashind on 23.04.15.
 */
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var _ = require('lodash');
var actions = module.exports;

function updateSocket(sid, admin, logger) {
    admin.socketId = sid;
    admin.save(function (err) {
        if (err) {
            return logger.error('Fail to update socket: ', admin);
        }
        logger.info('admin updated!: ', sid);
    });
}

actions.login = async(function (req, res) {
    console.log('login: ', req.body);
    var Admin = req.app.services.db.model('Admin'),
        username = (req.session.admin && req.session.admin.username) || req.body.username,
        admin = await(Admin.findOne({username: username}).exec()),
        sid = req.cookies['connect.sid'];

    if (req.session.admin) {
        updateSocket(sid, admin, req.app.services.logger);
        return res.send(admin);
    }
    if (!admin || (admin.password && admin.password !== req.body.password)) {
        return res.sendStatus(401);
    }
    updateSocket(sid, admin, req.app.services.logger);
    req.session.admin = admin;
    res.send(admin);
});

actions.update = async(function (req, res) {
    var Admin = req.app.services.db.model('Admin'),
        admin = await(Admin.findOne({_id: req.body._id}).exec());

    req.app.services.tools.assignModel(admin, req.body);
    admin.save(function (err) {
        if (err) {
            res.status(400);
            return res.send(err);
        }
        req.session.admin = admin;
        res.send(admin);
    });
});

actions.isAdmin = function (req, res, next) {
    if (!req.session.admin) {
        res.sendStatus(401);
    }
    next();
};