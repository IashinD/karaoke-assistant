/**
 * Created by shin on 19.04.2015.
 */
var _ = require('lodash'),
    async = require('asyncawait/async'),
    await = require('asyncawait/await');

module.exports.upload = async(function (req, res) {
    var Song = req.app.services.db.model('Song'),
        Artist = req.app.services.db.model('Artist'),
        songs = req.body;

    _.map(songs, function (song) {
        var artist = await(Artist.findOne({title: song.artist}).exec());
        if (!artist) {
            artist = await(Artist.new(song.artist));
        }
        song.artist = artist._id;
        Song.addSong(song);
    });
    res.sendStatus(202);
});