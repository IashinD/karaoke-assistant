/**
 * Created by iashind on 09.04.15.
 */
var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    _ = require('lodash'),
    actions = module.exports;

actions.start = async(function (req, res) {
    var Party = req.app.services.db.model('Party'),
        notClosed = await(Party.find({admin: req.params.admin, end: 0}).exec()),
        party = notClosed.length ? notClosed[0] : Party.new(req.params.admin);

    res.send(party);
});

actions.endParty = async(function (req, res) {
    var Party = req.app.services.db.model('Party'),
        party = await(Party.findOne({_id: req.params.id}).exec());

    Party.end(party);
    res.sendStatus(202);
});

actions.order = async(function (req, res) {
    var Order = req.app.services.db.model('Order'),
        order = Order.new(req.body);

    res.send(order);
});

actions.updateOrder = async(function (req, res) {
    var Order = req.app.services.db.model('Order'),
        order = await(Order.findOne({_id: req.body._id}).exec());

    req.body.passed = order.passed;
    req.app.services.tools.assignModel(order, req.body);
    order.save(function (err) {
        if (err) {
            return res.sendStatus(500);
        }
        res.send(order);
    });
});

actions.setPassed = async(function (req, res) {
    var Order = req.app.services.db.model('Order'),
        order = await(Order.findOne({_id: req.body._id}).exec());

    order.passed = req.body.passed;
    order.save(async(function (err) {
        var Client = req.app.services.db.model('Client'),
            client;

        if (err) {
            return res.sendStatus(500);
        }
        client = await(Client.findOne({_id: order.clientId}).populate('orders').populate('userId').exec());
        req.app.services.socket.pushMessage({
            data: client,
            evt: 'event',
            clientId: client._id,
            app: req.app
        }, req.app.services.logger);
        res.send(order);
    }));
});