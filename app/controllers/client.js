/**
 * Created by iashind on 10.04.15.
 */
var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    _ = require('lodash'),
    actions = module.exports;

function syncClient(client, socketId, app) {
    var adminSocket = app.sockets[socketId];

    if (!adminSocket) {
        throw new Error('admin socket not connected: ' + socketId);
    }
    adminSocket.send(JSON.stringify({
        event: 'event',
        data: client
    }), {compress: false});
}

function trySync(client, adminId, app, count, logger) {
    var timeout;
    if (!count) {
        count = 1;
    }
    timeout = 1000 * (count < 50 ? count : 50);
    try {
        syncClient(client, adminId, app);
    } catch (err) {
        logger.error('can\'t send message to admin ', adminId, err, timeout);

        setTimeout(function () {
            trySync(client, adminId, app, ++count, logger);
        }, timeout);
    }
}

actions.isLeft = function (req, res, next) {
    var Client = req.app.services.db.model('Client');
    if (!req.session.client) {
        return next();
    }

    Client.findOne({_id: req.session.client._id}).exec(function (err, client) {
        if (err) {
            res.status(500);
            return res.send(err);

        }
        if (!client || client.isLeft) {
            delete req.session.client;
            return res.sendStatus(401);
        }
        next();
    });
};

actions.getById = async(function (req, res) {
    var Client = req.app.services.db.model('Client'),
        client = await(Client.findOne({lastFive: req.params.id}).populate('orders').populate('userId').exec());

    if (!client || client.isLeft) {
        return res.sendStatus(401);
    }
    client.socketId = req.cookies['connect.sid'];
    client.save(function (err) {
        if (err) {
            return req.app.services.logger.error('error while updating client socket id: ', client, err);
        }
        if (!req.session.admin) {
            req.session.client = client;
        }
        res.send(client);
    });
});

actions.addClient = async(function (req, res) {
    var partyId = req.body.partyId,
        Client = req.app.services.db.model('Client'),
        client = await(Client.new(partyId)),
        Party = req.app.services.db.model('Party');

    Party.addClient(client, partyId);
    res.send(client);
});

actions.updateClient = async(function (req, res) {
    var data = req.body,
        Client = req.app.services.db.model('Client'),
        client;

    _.map(data.orders, function (order, key) {
        data.orders[key] = order._id;
    });

    client = await(Client.findOne({_id: data._id}).exec());

    if (typeof data.userId === 'object' && data.userId !== null) {
        data.userId = data.userId._id;
    }
    req.app.services.tools.assignModel(client, data);

    client.save(async(function (err) {
        var Party, party;
        if (err) {
            req.app.services.logger.error('error while saving updated client: ', client);
            req.app.services.logger.error('error : ', err);
            res.status(500);
            return res.send(err);
        }
        client = await(Client.findOne({_id: req.body._id}).populate('orders').populate('userId').exec());
        if (!req.session.admin) {
            req.session.client = client;
        }

        Party = req.app.services.db.model('Party');
        party = await(Party.findOne({_id: client.partyId}).exec());
        req.app.services.socket.pushMessage({
            data: client,
            evt: 'event',
            adminId: party.admin,
            app: req.app
        }, req.app.services.logger);
        res.send(client);
    }));
});

actions.getClients = async(function (req, res) {
    var Client = req.app.services.db.model('Client'),
        Song = req.app.services.mysql.models.Song,
        clients = await(Client.find({partyId: req.query.partyId, isLeft: false}).populate('userId').populate('orders').lean().exec());

    _.map(clients, function (client) {
        _.map(client.orders, function (order) {
            order.song = await(Song.getOne(order.songId));
        });
    });
    res.send(clients);
});
