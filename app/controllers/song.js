/**
 * Created by shin on 11.04.2015.
 */
var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    _ = require('lodash'),
    actions = module.exports;

actions.getOne = async(function (req, res) {
    var Song = req.app.services.mysql.models.Song,
        response = await(Song.getOne(req.params.id));

    res.send(response);
});

actions.getList = async(function (req, res) {
    var Song = req.app.services.mysql.models.Song,
        response = await(Song.getList(req.query));

    res.send(response);
});

actions.upload = async(function (req, res) {
    var data = req.body,
        Artist = req.app.services.mysql.models.Artist,
        Song = req.app.services.mysql.models.Song;

    data = await(Artist.populateData(data));
    res.send(await(Song.update(data)));
});