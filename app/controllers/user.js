/**
 * Created by shin on 11.04.2015.
 */
var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    _ = require('lodash'),
    actions = module.exports;

actions.regUser = async(function (req, res) {
    var User = req.app.services.db.model('User');

    User.reg(req.body).then(function (user) {
        res.send(user);
    }, function (err) {
        res.status(400);
        res.send(err);
    });
});

actions.check = async(function (req, res) {
    var User = req.app.services.db.model('User'),
        user = await(User.login(req.body));

    res.send(user);
});

actions.updateUser = async(function (req, res) {
    var data = req.body,
        User = req.app.services.db.model('User'),
        user = await(User.findOne({_id: data._id}).exec());

    req.app.services.tools.assignModel(user, data);
    user.save();
    res.send(user);
});

actions.getUser = async(function (req, res) {
    var User = req.app.services.db.model('User'),
        user = await(User.findOne({_id: req.params.id}).exec());

    res.send(user);
});