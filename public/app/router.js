/**
 * Created by iashind on 08.04.15.
 */
(function (ng, $) {



    function Config($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
        function Interceptor($q, $location) {
            return {
                responseError: function (response) {
                    if (response.status === 401 || response.status === 403) {
                        window.localStorage.removeItem('cid');
                        $location.url('/start');
                    }
                    return $q.reject(response);
                }
            };
        }
        Interceptor.$inject = ['$q', '$location'];
        $httpProvider.interceptors.push(Interceptor);

        function getClient(Api, $state) {
            var def = $.Deferred(),
                cid = window.localStorage.getItem('cid');

            if (!cid) {
                def.resolve($state.go('start'));
            }
            Api.Clients.get({id: cid}).$promise.then(function (client) {
                def.resolve(client);
            }).catch(function () {
                def.resolve($state.go('start'));
            });
            return def.promise();
        }
        getClient.$inject = ['Api', '$state'];

        function getAdmin(Api) {
            var def = $.Deferred();
            Api.Admin.login({username: 'nevermind'}).$promise.then(function (admin) {
                def.resolve(admin);
            }).catch(function () {
                def.resolve({});
            });
            return def.promise();
        }
        getAdmin.$inject = ['Api'];

        $stateProvider
            .state('superadmin', {
                url: '/superadmin',
                templateUrl: 'app/modules/admin/superadmin.html',
                controller: 'SuperAdminCtrl',
                controllerAs: 'SuperCtrl'
            })
            .state('client', {
                templateUrl: 'app/modules/client/client.html',
                controller: 'ClientCtrl',
                controllerAs: 'ClientCtrl',
                resolve: {
                    client: getClient
                }
            })
            .state('client.login', {
                url: '/login',
                templateUrl: 'app/modules/auth/login.html',
                controller: 'LoginCtrl',
                controllerAs: 'LoginCtrl'
            })
            .state('client.catalog', {
                url: '/catalog',
                templateUrl: 'app/modules/client/catalog/catalog.html',
                controller: 'CatalogCtrl',
                controllerAs: 'CatalogCtrl'
            })
            .state('client.orders', {
                url: '/orders',
                templateUrl: 'app/modules/client/orders/orders.html',
                controller: 'OrdersCtrl',
                controllerAs: 'OrdersCtrl'
            })
            .state('admin', {
                abstract: true,
                template: '<div ui-view></div>',
                resolve: {
                    admin: getAdmin
                }
            })
            .state('start', {
                url: '/start',
                templateUrl: 'app/modules/auth/start.html',
                controller: 'StartCtrl',
                controllerAs: 'StartCtrl'
            })
            .state('client.party', {
                url: '/party',
                templateUrl: 'app/modules/party/party.html',
                controller: 'PartyCtrl',
                controllerAs: 'PartyCtrl'
            })
            .state('client.party.list', {
                url: '/list',
                templateUrl: 'app/modules/party/list.html',
                controller: 'ListCtrl',
                controllerAs: 'ListCtrl'
            })
            .state('404', {
                url: '/404',
                templateUrl: 'app/modules/errors/404.html'
            })
            .state('admin.manager', {
                url: '/manager',
                templateUrl: 'app/modules/manager/manager.html',
                controller: 'ManagerCtrl',
                controllerAs: 'ManagerCtrl'
            });
        $urlRouterProvider.otherwise('start');
        $locationProvider.html5Mode({
            enabled: true
        });

    }
    Config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider'];

    ng.module('app').config(Config);
}(window.angular, window.$));