/**
 * Created by iashind on 25.05.15.
 */
(function (ng, WebSocket, _) {

    function Socket() {
        this.listeners = [];
        this.init();
    }

    Socket.prototype.init = function (triesCount) {
        var host = location.origin.replace(/^http/, 'ws'),
            ctrl = this,
            timeout;

        triesCount = triesCount || 1;
        this.ws = new WebSocket(host);

        this.ws.onclose = function () {
            timeout = triesCount <= 60 ? 1000 * triesCount : 60000;
            setTimeout(function () {
                ctrl.init(++triesCount);
            }, timeout);

        };

        _.map(this.listeners, this.listen.bind(this));
    };

    Socket.prototype.addListener = function (event, callback) {
        var listener = {
            event: event,
            callback: callback
        };

        this.listeners.push(listener);
        this.listen(listener);
    };

    Socket.prototype.listen = function (listener) {
        this.ws.onmessage = function (resp) {
            var data = ng.fromJson(resp.data);
            if (data.event === listener.event) {
                listener.callback(data.data);
            }
        };
    };

    ng.module('app').service('Socket', Socket);
}(window.angular, window.WebSocket, window._));