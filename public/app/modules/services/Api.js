/**
 * Created by iashind on 17.04.15.
 */
(function (ng) {

    function Api($resource) {
        this.Order = $resource('/api/party/order/', {},
            {
                create: {
                    method: 'post',
                    url: 'api/order'
                },
                cancel: {method: 'delete'},
                update: {
                    method: 'put',
                    url: 'api/order/:id',
                    params: {
                        id: '@_id'
                    }
                },
                setPassed: {
                    method: 'put',
                    url: 'api/manager/order/:id',
                    params: {
                        id: '@_id'
                    }
                }
            }
        );
        this.Song = $resource('/api/songs', {}, {
            getOne: {
                method: 'get',
                url: '/api/songs/:id',
                params: {
                    id: '@id'
                }
            }
        });
        this.Party = $resource('/api/party/:admin', {admin: '@admin'}, {
            endParty: {
                method: 'put',
                url: '/api/party/:id',
                params: {
                    id: '@id'
                }
            }
        });
        this.Clients = $resource('/api/clients/:id', {id: '@id'}, {
            update: {
                method: 'put',
                url: '/api/clients/'
            }
        });
        this.User = $resource('/api/user/:id', {id: '@id'}, {
            getAuth: {
                method: 'post',
                url: '/api/user/:email',
                params: {
                    email: '@email'
                }
            },
            update: {
                method: 'put',
                url: 'api/user/:id',
                params: {
                    id: '@id'
                }
            }
        });

        this.Admin = $resource('/admin', {}, {
            login: {
                method: 'post',
                url: '/admin/:username',
                params: {
                    username: '@username'
                }
            },
            update: {
                method: 'put',
                url: '/admin/:id',
                params: {
                    id: '@_id'
                }
            }
        });

        this.Uploader = $resource('/api/upload');
    }
    Api.$inject = ['$resource'];
    ng.module('app').service('Api', Api);
}(window.angular));