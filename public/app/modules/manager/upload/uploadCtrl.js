/**
 * Created by iashind on 20.04.15.
 */
(function (ng, FileReader, XLS, _) {

    function UploadCtrl(Api, $modalInstance) {
        this.Uploader = Api.Uploader;
        this.$modalInstance = $modalInstance;
    }
    UploadCtrl.$inject = ['Api', '$modalInstance'];

    UploadCtrl.prototype.parseXLS = function () {
        var ctrl = this,
            files = this.files,
            reader,
            concatData = [],
            i;

        this.parsing = true;
        function onError(e) {
            console.log(e);
            concatData.push([]);
        }
        function onLoad(e) {
            var data = e.target.result,
                workbook = XLS.read(data, {type: 'binary'}),
                sheetName = workbook.SheetNames[0],
                json = XLS.utils.sheet_to_json(workbook.Sheets[sheetName], {
                    header: 'A'
                }),
                row;

            for (i = json.length - 1; i >= 0; i--) {
                row = json[i];
                json[i] = [+row.A, row.B, row.C, !!row.D, !!row.E];
            }
            concatData.push(json);
            if (concatData.length === files.length) {
                ctrl.uploadData(_.flatten(concatData));
            }
        }

        for (i = 0; i < files.length; i++) {
            reader = new FileReader();
            reader.onload = onLoad;
            reader.onerror = onError;
            reader.readAsBinaryString(files[i]);
        }
    };

    UploadCtrl.prototype.uploadData = function (data) {
        var ctrl = this,
            totalRows = data.length;

        this.Uploader.save(data).$promise.then(
            function (res) {
                ctrl.files = [];
                ctrl.response = {
                    total: totalRows,
                    affected: res.affectedRows,
                    duplicated: totalRows - res.affectedRows,
                    errors: 0
                };
                ctrl.parsing = false;
            }
        );
    };

    UploadCtrl.prototype.close = function () {
        this.$modalInstance.close();
    };
    ng.module('app').controller('UploadCtrl', UploadCtrl);
}(window.angular, window.FileReader, window.XLS, window._));