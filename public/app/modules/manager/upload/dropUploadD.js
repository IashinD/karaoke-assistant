/**
 * Created by iashind on 27.04.15.
 */
(function (ng) {
    function Directive() {
        function preventDefault(e) {
            e.preventDefault();
            e.stopPropagation();
        }
        return {
            restrict: 'A',
            scope: {
                dropUpload: '='
            },
            link: function ($scope, $el) {
                document.body.addEventListener("dragover", preventDefault);
                document.body.addEventListener("dragleave", preventDefault);
                document.body.addEventListener("drop", preventDefault);
                $el[0].addEventListener("dragover", function (e) {
                    preventDefault(e);
                    $el.addClass('drop-hover');
                }, false);
                $el[0].addEventListener("dragleave", function (e) {
                    preventDefault(e);
                    $el.removeClass('drop-hover');
                }, false);
                $el[0].addEventListener('drop', function (e) {
                    var i, files;
                    preventDefault(e);
                    $el.removeClass('drop-hover');
                    files = e.target.files || e.dataTransfer.files;
                    if (!$scope.dropUpload) {
                        $scope.dropUpload = [];
                    }
                    for (i = 0; i < files.length; i++) {
                        if (files[i].type === "application/vnd.ms-excel") {
                            $scope.dropUpload.push(files[i]);
                        }
                    }
                    try {
                        $scope.$apply();
                    } catch (err) {
                        console.log(err);
                    }
                });
            }
        };
    }


    ng.module('app').directive('dropUpload', Directive);
}(window.angular));