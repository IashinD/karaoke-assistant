/**
 * Created by shin on 19.04.2015.
 */
(function (ng) {
    function Upload() {
        return {
            scope: {
                upload: "="
            },
            link: function ($scope, $el) {
                $el.on("change", function (event) {
                    var files = event.target.files,
                        i;

                    if (!$scope.upload) {
                        $scope.upload = [];
                    }

                    $scope.$apply(function () {
                        for (i = 0; i < files.length; i++) {
                            $scope.upload.push(files[i]);
                        }
                    });
                });
            }
        };
    }
    ng.module('app').directive('upload', Upload);
}(window.angular));