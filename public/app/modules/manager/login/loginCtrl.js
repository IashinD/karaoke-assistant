/**
 * Created by iashind on 23.04.15.
 */
(function (ng, md5) {

    function LoginCtrl($modalInstance, Api) {
        this.$modalInstance = $modalInstance;
        this.Admin = Api.Admin;
    }
    LoginCtrl.$inject = ['$modalInstance', 'Api'];

    LoginCtrl.prototype.login = function (username, password) {
        var ctrl = this;

        ctrl.wrongPair = false;
        this.Admin.login({
            username: username,
            password: password ? md5(password) : ''
        }).$promise.then(function (response) {
            ctrl.$modalInstance.close(response);
        }).catch(function () {
            ctrl.wrongPair = true;
        });
    };
    ng.module('app').controller('ManagerLoginCtrl', LoginCtrl);
}(window.angular, window.md5));