/**
 * Created by iashind on 09.04.15.
 */
(function (ng, WebSocket, toastr, _) {
    function updateExisting(oldCollection, newCollection) {
        _.map(oldCollection, function (item, i) {
            _.map(newCollection, function (newItem) {
                if (newItem._id === item._id) {
                    newItem.collapsed = oldCollection[i].collapsed;
                    newItem.title = oldCollection[i].title;
                    newItem.hidePassed = oldCollection[i].hidePassed;
                    oldCollection[i] = newItem;
                }
            });
        });
    }

    function addNewItems(oldCollection, newCollection) {
        _.map(newCollection, function (newItem) {
            var temp = _.find(oldCollection, function (item) {
                return item._id === newItem._id;
            });
            if (!temp) {
                oldCollection.push(newItem);
            }
        });
    }

    function removeDeleted(oldCollection, newCollection) {
        var i;
        function findById(newItem) {
            return oldCollection[i]._id === newItem._id;
        }
        for (i = oldCollection.length - 1; i >= 0; i--) {
            if (!_.find(newCollection, findById)) {
                oldCollection.splice(i, 1);
            }
        }
    }
    function AdminCtrl(Api, $scope, $modal, admin, $http, $timeout, Socket) {
        this.$modal = $modal;
        this.Party = Api.Party;
        this.Clients = Api.Clients;
        this.Order = Api.Order;
        this.$scope = $scope;
        this.Uploader = Api.Uploader;
        this.$http = $http;
        this.$timeout = $timeout;
        this.socket = Socket;

        if (!admin._id) {
            this.showLoginModal();
        } else {
            this.init(admin);
        }
    }
    AdminCtrl.$inject = ['Api', '$scope', '$modal', 'admin', '$http', '$timeout', 'Socket'];

    AdminCtrl.prototype.showLoginModal = function () {
        var ctrl = this,
            modal = this.$modal.open({
                templateUrl: 'app/modules/manager/login/login.html',
                controller: 'ManagerLoginCtrl',
                controllerAs: 'ctrl'
            });

        modal.result.then(function (admin) {
            ctrl.init(admin);

        });
    };

    AdminCtrl.prototype.showPasswordResetModal = function () {
        var ctrl = this,
            modal = this.$modal.open({
                templateUrl: 'app/modules/manager/passwordReset/passwordReset.html',
                controller: 'PasswordResetCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    admin: function () {
                        return ctrl.admin;
                    }
                }
            });

        modal.result.then(function (admin) {
            ctrl.init(admin);
        });
    };

    AdminCtrl.prototype.init = function (admin) {
        var ctrl = this;
        ctrl.admin = admin;
        ctrl.isManager = true;

        if (admin.temp) {
            return this.$timeout(function () {
                ctrl.showPasswordResetModal(admin);
            }, 1000);
        }

        if (window.localStorage.getItem('partyId')) {
            this.startParty();
        } else {
            this.ready = true;
        }
        this.socket.addListener('event', function (data) {
            var action = '',
                nick,
                i;

            ctrl.updateClients();
            for (i = 0; i < ctrl.clients.length; i++) {
                if (ctrl.clients[i]._id === data._id) {
                    if (data.isLeft) {
                        action = 'Client left: ';
                    } else if (ctrl.clients[i].orders.length > data.orders.length) {
                        action = 'Order removed: ';
                    } else if (ctrl.clients[i].orders.length < data.orders.length) {
                        action = 'Order added: ';
                    } else if (!ctrl.clients[i].isActive && data.isActive) {
                        action = 'Client activated: ';
                    } else if (!ctrl.clients[i].userId && data.userId) {
                        action = 'Client login: ';
                    } else if (ctrl.clients[i].userId && !data.userId) {
                        action = 'Client logout: ';
                    } else {
                        action = 'Orders updated: ';
                    }
                    nick = ctrl.clients[i].title || (data.userId ? data.userId.nickname || data.userId.email : data.lastFive);
                    break;
                }
            }

            toastr.info(action + nick, 'Server message', {
                closeButton: true,
                preventDuplicates: true,
                progressBar: true
            });
        });
    };

    AdminCtrl.prototype.startParty = function () {
        var ctrl = this;
        this.party = this.Party.get({admin: this.admin._id});
        this.party.$promise.then(function () {
            window.localStorage.setItem('partyId', ctrl.party._id);
            ctrl.updateClients();
        });
    };

    AdminCtrl.prototype.updateClients = function () {
        var ctrl = this;
        this.Clients.query({partyId: this.party._id}).$promise.then(function (clients) {
            var i;

            for (i = 0; i < clients.length; i++) {
                clients[i].collapsed = true;
            }

            if (!ctrl.clients) {
                ctrl.clients = clients;
                return;
            }

            removeDeleted(ctrl.clients, clients);
            updateExisting(ctrl.clients, clients);
            addNewItems(ctrl.clients, clients);
        });
    };

    AdminCtrl.prototype.endParty = function () {
        var ctrl = this;
        this.Party.endParty({id: this.party._id}).$promise.then(function() {
            ctrl.party = undefined;
            ctrl.clients = undefined;
            window.localStorage.removeItem('partyId');
        });
    };

    AdminCtrl.prototype.addClient = function () {
        var ctrl = this,
            client = this.Clients.save({partyId: this.party._id});

        client.$promise.then(function (client) {
            ctrl.party.clients.push(client);
            ctrl.updateClients();
        });

    };

    AdminCtrl.prototype.orderPassed = function (order) {
        this.Order.setPassed(order);
    };

    AdminCtrl.prototype.showUploadModal = function () {
        this.$modal.open({
            templateUrl: 'app/modules/manager/upload/upload.html',
            controller: 'UploadCtrl',
            controllerAs: 'ctrl'
        });
    };

    AdminCtrl.prototype.leave = function (client) {
        var modal = this.$modal.open({
                templateUrl: 'app/modules/common/confirm.html',
                controller: 'ConfirmCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    question: function () {
                        return 'Are you sure you want to delete client?';
                    }
                }
            });

        modal.result.then(function (confirmed) {
            if (!confirmed) {
                return;
            }
            client.isLeft = true;
            client.$update();
        });

    };
    ng.module('app').controller('ManagerCtrl', AdminCtrl);
}(window.angular, window.WebSocket, window.toastr, window._));