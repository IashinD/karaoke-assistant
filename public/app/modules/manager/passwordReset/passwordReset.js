/**
 * Created by iashind on 28.05.15.
 */
(function (ng, _, md5) {
    function Controller($modalInstance, admin, $scope, $timeout, Api) {
        this.admin = admin;
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.Admin = Api.Admin;
        this.$modalInstance = $modalInstance;
        this.addValidation();
    }
    Controller.$inject = ['$modalInstance', 'admin', '$scope', '$timeout', 'Api'];

    Controller.prototype.addValidation = function () {
        var ctrl = this;
        if (!this.$scope.adminForm) {
            return this.$timeout(function () {
                ctrl.addValidation();
            }, 100);
        }
        this.$scope.adminForm.rptPassword.$validators.match = function (value) {
            return value === ctrl.$scope.adminForm.password.$modelValue;
        };
    };

    Controller.prototype.updateUser = function () {
        var ctrl = this;
        this.alreadyExist = false;
        _.assign(this.admin, {
            username: this.$scope.username,
            password: md5(this.$scope.password),
            temp: false
        });
        this.Admin.update(this.admin).$promise.then(function (admin) {
            _.assign(ctrl.admin, admin);
            ctrl.$modalInstance.close(admin);
            console.log(admin);
        }).catch(function (err) {
            ctrl.alreadyExist = true;
            console.log(err);
        });
    };

    ng.module('app').controller('PasswordResetCtrl', Controller);
}(window.angular, window._, window.md5));