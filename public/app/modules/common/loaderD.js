/**
 * Created by iashind on 22.05.15.
 */
(function (ng) {
    function Loader() {
        return {
            restrict: 'A',
            link: function link($scope, $el) {
                $el.addClass('hidden');
            }
        };
    }

    ng.module('app').directive('loader', Loader);
}(window.angular));