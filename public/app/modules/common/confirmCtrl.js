/**
 * Created by iashind on 19.05.15.
 */
(function (ng) {
    function ConfirmCtrl($modalInstance, $scope, question) {
        $scope.question = question;
        this.$modalInstance = $modalInstance;
    }
    ConfirmCtrl.$inject = ['$modalInstance', '$scope', 'question'];

    ConfirmCtrl.prototype.ok = function () {
        this.$modalInstance.close(true);
    };

    ConfirmCtrl.prototype.close = function () {
        this.$modalInstance.dismiss();
    };
    ng.module('app').controller('ConfirmCtrl', ConfirmCtrl);
}(window.angular));