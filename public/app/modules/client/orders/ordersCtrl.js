/**
 * Created by iashind on 14.05.15.
 */
(function (ng, _) {

    function OrdersCtrl($scope, Api, $modal) {
        var ctrl = this;
        this.$modal = $modal;
        this.client = $scope.client;
        this.orders = [];
        this.Song = Api.Song;
        this.Clients = Api.Clients;
        this.Order = Api.Order;
        this.$scope = $scope;
        _.map(this.client.orders, function (order) {
            if (order.passed) {
                return;
            }
            ctrl.Song.getOne({id: order.songId}).$promise.then(function (song) {
                ctrl.orders.push({
                    song: song,
                    order: order
                });
            });
        });

        $scope.$on('sync', this.syncOrders.bind(this));
    }
    OrdersCtrl.$inject = ['$scope', 'Api', '$modal'];

    OrdersCtrl.prototype.delayUpdate = function () {
        var ctrl = this;

        clearTimeout(this.timeout);
        this.timeout = setTimeout(function () {
            ctrl.updateClient();
        }, 3000);
    };

    OrdersCtrl.prototype.syncOrders = function () {
        var ctrl = this;
        _.map(ctrl.client.orders, function (order) {
            _.map(ctrl.orders, function (row) {
                if (order._id === row.order._id) {
                    row.order.passed = order.passed;
                }
            });
        });
        try {
            this.$scope.$apply();
        } catch (err) {
            console.log(err);
        }
    };

    OrdersCtrl.prototype.increase = function (tune, range) {
        if (tune < range) {
            this.delayUpdate();
            return tune += 0.5;
        }
        return tune;
    };

    OrdersCtrl.prototype.decrease = function (tune, range) {
        if (tune > range) {
            this.delayUpdate();
            return tune -= 0.5;
        }
        return tune;
    };

    OrdersCtrl.prototype.orderUp = function (row) {
        var index = this.orders.indexOf(row);
        if (index === 0) {
            return;
        }
        this.orders.splice(index, 1);
        this.orders.splice(index - 1, 0, row);
        this.delayUpdate();
    };

    OrdersCtrl.prototype.orderDown = function (row) {
        var index = this.orders.indexOf(row);
        if (index === this.orders.length) {
            return;
        }
        this.orders.splice(index, 1);
        this.orders.splice(index + 1, 0, row);
        this.delayUpdate();
    };

    OrdersCtrl.prototype.updateClient = function () {
        var ctrl = this,
            i;

        _.map(ctrl.orders, function (row) {
            ctrl.Order.update(row.order);
        });
        for (i = ctrl.client.orders.length - 1; i >= 0; i--) {
            if (!ctrl.client.orders[i].passed) {
                ctrl.client.orders.splice(i, 1);
            }
        }
        for (i = 0; i < this.orders.length; i++) {
            this.client.orders.push(this.orders[i].order);
        }
        this.Clients.update(this.client);
    };

    OrdersCtrl.prototype.deleteOrder = function (row) {
        var ctrl = this,
            modal = this.$modal.open({
            templateUrl: 'app/modules/common/confirm.html',
            controller: 'ConfirmCtrl',
            controllerAs: 'ctrl',
            resolve: {
                question: function () {
                    return 'Are you sure you want to delete this order?';
                }
            }
        });

        modal.result.then(function (confirmed) {
            if (!confirmed) {
                return;
            }
            var index = ctrl.orders.indexOf(row);
            ctrl.orders.splice(index, 1);
            ctrl.delayUpdate();
        });

    };

    ng.module('app').controller('OrdersCtrl', OrdersCtrl);
}(window.angular, window._));