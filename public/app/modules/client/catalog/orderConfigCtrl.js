/**
 * Created by iashind on 21.04.15.
 */
(function (ng) {

    function OrderConfig($modalInstance, data, Api) {
        this.song = data.song;
        this.user = data.user;
        this.$modalInstance = $modalInstance;
        this.User = Api.User;

        if (this.user) {
            this.tunes = this.user.tunes[data.song.id];
        } else {
            this.tunes = {
                tone: 0,
                speed: 0
            };
        }
    }
    OrderConfig.$inject = ['$modalInstance', 'data', 'Api', '$rootScope'];

    OrderConfig.prototype.increase = function (tune, range) {
        tune += 0.5;
        if (tune > range) {
            tune = range;
        }
        return tune;
    };

    OrderConfig.prototype.decrease = function (tune, range) {
        tune -= 0.5;
        if (tune < range) {
            tune = range;
        }
        return tune;
    };

    OrderConfig.prototype.close = function () {
        this.$modalInstance.dismiss();
    };

    OrderConfig.prototype.saveTunes = function () {
        var ctrl = this;
        if (!ctrl.user.tunes) {
            ctrl.user.tunes = {};
        }
        ctrl.user.tunes[this.song.id] = this.tunes;
        ctrl.User.update(ctrl.user);
    };

    OrderConfig.prototype.makeOrder = function () {
        if (this.saveAsDef) {
            this.saveTunes();
        }
        this.$modalInstance.close(this.tunes);
    };
    ng.module('app').controller('OrderConfigCtrl', OrderConfig);
}(window.angular));