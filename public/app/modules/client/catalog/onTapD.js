(function (ng) {
    function OnTapD() {
        return {
            restrict: 'A',
            scope: {
                onTap: '&',
                onTapHold: '&'
            },
            link: function ($scope, $el) {
                $el.on('tap', $scope.onTap);
                $el.on('taphold', $scope.onTapHold);
            }
        };
    }
    ng.module('app').directive('onTap', OnTapD);
}(window.angular));
