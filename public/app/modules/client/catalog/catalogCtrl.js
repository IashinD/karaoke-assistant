/**
 * Created by iashind on 14.05.15.
 */
(function (ng, toastr, $, _) {

    function updateOrdered(orders, songs) {
        _.map(songs, function (song) {
            _.map(orders, function (order) {
                if (order.songId === song.id) {
                    if (order.passed) {
                        song.isOrdered = false;
                    } else {
                        song.isOrdered = true;
                    }
                }
            });
        });
    }

    function CatalogCtrl($scope, $modal, Api, $rootScope) {
        this.songs = $scope.songs;
        this.getSongs = $scope.getSongs;
        this.client = $scope.client;
        this.$modal = $modal;
        this.Clients = Api.Clients;
        this.User = Api.User;
        this.Order = Api.Order;
        this.getSongs();
        $rootScope.$on('sync', function () {
            console.log('sync');
        });
    }
    CatalogCtrl.$inject = ['$scope', '$modal', 'Api', '$rootScope'];

    CatalogCtrl.prototype.configOrder = function (song) {
        var ctrl = this,
            modal;

        this.isConfig = true;
        if (this.isOrdersLimitReached()) {
            return;
        }

        if (song.isOrdered) {
            return;
        }

        modal = this.$modal.open({
            templateUrl: 'app/modules/client/catalog/order-config.html',
            controller: 'OrderConfigCtrl',
            controllerAs: 'ctrl',
            resolve: {
                data: function () {
                    return {
                        song: song,
                        user: ctrl.client.userId
                    };
                }
            }
        });

        modal.result.then(function (tunes) {
            ctrl.isConfig = false;
            ctrl.makeOrder(song, tunes);
        });
    };

    CatalogCtrl.prototype.isOrdersLimitReached = function () {
        var orders = this.client.orders,
            i,
            actual = 0;

        for (i = 0; i < orders.length; i++) {
            if (orders[i].passed) {
                continue;
            }
            actual++;
        }
        if (actual < 10) {
            return false;
        }
        toastr.warning('Delete some orders or wait until they pass.', 'Orders limit reached.', {
            closeButton: true,
            preventDuplicates: true,
            progressBar: true
        });
        return true;
    };

    CatalogCtrl.prototype.makeOrder = function (song, tunes) {
        var ctrl = this,
            order;

        if (this.isConfig) {
            return;
        }
        if (song.isOrdered) {
            return ctrl.deleteOrder(song);
        }

        if (this.isOrdersLimitReached()) {
            return;
        }

        order = {
            songId: song.id,
            clientId: this.client._id
        };

        if (tunes) {
            order.speed = tunes.speed;
            order.tone = tunes.tone;
        } else if (this.client.userId && this.client.userId.tunes && this.client.userId.tunes[song.id]) {
            order.speed = this.client.userId.tunes[song.id].speed;
            order.tone = this.client.userId.tunes[song.id].tone;
        }
        this.Order.create(order).$promise.then(function (resp) {
            ctrl.client.orders.push(resp);
            song.isOrdered = true;
            ctrl.Clients.update(ctrl.client);
        });
    };

    CatalogCtrl.prototype.deleteOrder = function (song) {
        var orders = this.client.orders,
            i;

        for (i = 0; i < orders.length; i++) {
            if (orders[i].songId === song.id) {
                orders.splice(i, 1);
                song.isOrdered = false;
                this.Clients.update(this.client);
            }
        }
    };

    CatalogCtrl.prototype.addToFavs = function (song) {
        if (!this.client.userId) {
            return;
        }
        song.inFavs = !song.inFavs;
        if (song.inFavs) {
            this.client.userId.favs.push(song.id);
        } else {
            var index = this.client.userId.favs.indexOf(song.id);
            this.client.userId.favs.splice(index, 1);
        }
        this.User.update(this.client.userId);
    };
    ng.module('app').controller('CatalogCtrl', CatalogCtrl);
}(window.angular, window.toastr, window.$, window._));