/**
 * Created by iashind on 20.04.15.
 */
(function (ng, _, toastr) {

    function updateFavs(user, songs) {
        var favs;

        if (!user) {
            return;
        }
        favs = user.favs;
        _.map(songs, function (song) {
            if (~favs.indexOf(song.id)) {
                song.inFavs = true;
            }
        });
    }

    function updateOrdered(orders, songs) {
        _.map(songs, function (song) {
            _.map(orders, function (order) {
                if (order.songId === song.id) {
                    if (order.passed) {
                        song.isOrdered = false;
                    } else {
                        song.isOrdered = true;
                    }
                }
            });
        });
    }

    function ClientCtrl($state, client, Api, $scope, $rootScope, Socket) {
        this.socket = Socket;
        this.Clients = Api.Clients;
        this.Song = Api.Song;
        this.$state = $state;
        this.client = client;
        this.songs = [];
        this.filter = {
            nation: 3,
            pro: 0,
            page: 1,
            favorites: false
        };
        this.pagination = {
            totalItems: 0,
            itemsPerPage: 20,
            width: Math.floor((window.innerWidth - 111) / 53)
        };
        this.$rootScope = $rootScope;
        this.$scope = $scope;
        $scope.client = client;
        $scope.songs = this.songs;
        $scope.getSongs = this.getSongs.bind(this);
        this.activateClient();
        this.bindSocket();
    }
    ClientCtrl.$inject = ['$state', 'client', 'Api', '$scope', '$rootScope', 'Socket'];

    ClientCtrl.prototype.bindSocket = function () {
        var ctrl = this;
        this.socket.addListener('event', function (data) {
            _.assign(ctrl.client, data);
            updateOrdered(ctrl.client.orders, ctrl.songs);
            try {
                ctrl.$scope.$apply();
            } catch (err) {
                console.log(err);
            }
            toastr.info('order marked as passed', 'Server message', {
                closeButton: true,
                preventDuplicates: true,
                progressBar: true
            });
            ctrl.$rootScope.$broadcast('sync', data);
            console.log(data);
        });
    };

    ClientCtrl.prototype.activateClient = function () {
        if (!this.client.isActive) {
            this.client.isActive = true;
            this.client.$update();
        }
    };

    ClientCtrl.prototype.showNavbar = function () {
        return this.$state.current.name !== 'client.login';
    };

    ClientCtrl.prototype.showFilter = function () {
        return this.$state.current.name === 'client.catalog';
    };

    ClientCtrl.prototype.signOut = function () {
        this.client.userId = null;
        this.client.$update();
        this.$state.go('client.login');
    };

    ClientCtrl.prototype.getSongs = function () {
        var ctrl = this;
        this.songs.length = 0;
        ctrl.Song.get(this.filter).$promise.then(function (res) {
            updateOrdered(ctrl.client.orders, res.rows);
            updateFavs(ctrl.client.userId, res.rows);
            _.assign(ctrl.songs, res.rows);
            ctrl.pagination.totalItems = res.totalCount;
            ctrl.songsLoading = false;
        });
        this.songsLoading = true;
    };


    ClientCtrl.prototype.applyFilter = function () {
        this.filter.page = 1;
        if (this.filter.favorites) {
            this.filter.favs = this.client.userId.favs;
        } else {
            this.filter.favs = undefined;
        }
        this.getSongs();
    };

    ClientCtrl.prototype.showPagination = function () {
        return this.$state.current.name === 'client.catalog' && this.pagination.totalItems > this.pagination.itemsPerPage;
    };

    ClientCtrl.prototype.clearSearchString = function () {
        this.filter.searchString = '';
        this.applyFilter();
    };

    ng.module('app').controller('ClientCtrl', ClientCtrl);
}(window.angular, window._, window.toastr));