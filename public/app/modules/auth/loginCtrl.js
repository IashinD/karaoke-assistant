/**
 * Created by iashind on 08.04.15.
 */
(function (ng, md5) {

    function LoginCtrl(client, $modal, $state, Api, $location) {
        $location.url('/login');
        this.client = client;
        this.$modal = $modal;
        this.$state = $state;
        this.User = Api.User;
        this.Clients = Api.Clients;
        this.auth = true;
        this.nextRoute = 'client.catalog';
    }
    LoginCtrl.$inject = ['client', '$modal', '$state', 'Api', '$location'];

    LoginCtrl.prototype.reg = function (email, password) {
        var ctrl = this,
            modal = this.$modal.open({
                templateUrl: 'app/modules/auth/registration.html',
                controller: 'RegCtrl',
                controllerAs: 'ctrl',
                resolve: {
                    data: function () {
                        return {
                            email: email,
                            password: password
                        };
                    }
                }
            });

        modal.result.then(function (user) {
            ctrl.login(user);
        });
    };

    LoginCtrl.prototype.login = function (user) {
        if (!user) {
            return;
        }
        this.client.userId = user;
        this.Clients.update(this.client);
        this.next();
    };

    LoginCtrl.prototype.check = function (email, password) {
        var ctrl = this,
            user = this.User.getAuth({
                email: email,
                password: md5(password)
            });
        ctrl.auth = true;

        user.$promise.then(function () {
            if (!user._id) {
                ctrl.auth = false;
                return;
            }
            ctrl.login(user);
        }).catch( function(err) {
            if(err.code === 11000) {
                console.log('duplicated');
            }
        });
    };

    LoginCtrl.prototype.next = function () {
        this.$state.go(this.nextRoute);
    };

    ng.module('app').controller('LoginCtrl', LoginCtrl);
}(window.angular, window.md5));