/**
 * Created by shin on 10.04.2015.
 */
(function (ng, md5, _) {

    function ModalController($modalInstance, data, $scope, $sce, Api) {
        this.$sce = $sce;
        this.$modalInstance = $modalInstance;
        this.User = Api.User;
        this.$scope = $scope;
        $scope.data = data;
    }
    ModalController.$inject = ['$modalInstance', 'data', '$scope', '$sce', 'Api'];

    ModalController.prototype.close = function (data) {
        var ctrl = this;
        if (!data) {
            return this.$modalInstance.dismiss();
        }
        ctrl.duplicateEmail = false;
        data.password = md5(data.password);
        delete data.rptPassword;
        this.User.save(data).$promise.then(function (user) {
            ctrl.$modalInstance.close(user);
        }).catch(function (err) {
            delete data.password;
            if (err.data.code === 11000) {
                ctrl.duplicateEmail = true;
            }
        });
    };

    ModalController.prototype.tips = function (form) {
        var tips = '';
        _.map(form.$error, function (error, errorName) {
            switch (errorName) {
            case 'required':
                tips += 'All fields are required!<br />';
                break;
            case 'email':
                tips += 'Email is invalid!<br />';
                break;
            case 'minlength':
                tips += 'Password and Email must be at least 4 chars!<br />';
                break;
            }
        });

        if (form.password.$modelValue && form.rptPassword.$modelValue && form.password.$modelValue !== form.rptPassword.$modelValue) {
            tips += 'Passwords do not match!<br/>';
        }

        if (this.duplicateEmail) {
            tips += 'This email already registered!';
        }
        tips = this.$sce.trustAsHtml(tips);
        return tips;
    };

    ng.module('app').controller('RegCtrl', ModalController);
}(window.angular, window.md5, window._));