/**
 * Created by iashind on 08.04.15.
 */
(function (ng) {

    function StartCtrl(Api, $state) {
        this.Clients = Api.Clients;
        this.$state = $state;
    }
    StartCtrl.$inject = ['Api', '$state'];

    StartCtrl.prototype.checkClientId = function (clientid) {
        var ctrl = this;
        this.wrongId = false;
        this.Clients.get({id: clientid}).$promise.then(function () {
            window.localStorage.setItem('cid', clientid);
            ctrl.$state.go('client.login');
        }).catch(function (err) {
            console.log(err);
            ctrl.wrongId = true;
        });
    };

    StartCtrl.prototype.goToManager = function() {
        this.$state.go('admin.manager');
    };

    ng.module('app').controller('StartCtrl', StartCtrl);
}(window.angular));