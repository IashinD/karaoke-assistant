/**
 * Created by iashind on 23.04.15.
 */
(function (ng) {

    function LoginCtrl($modalInstance) {
        this.$modalInstance = $modalInstance;
    }
    LoginCtrl.$inject = ['$modalInstance'];

    LoginCtrl.prototype.login = function (code) {
        this.$modalInstance.close(code);
    };

    ng.module('app').controller('SuperLoginCtrl', LoginCtrl);
}(window.angular));
