/**
 * Created by iashind on 23.04.15.
 */
(function (ng, md5) {

    function SuperAdminCtrl($modal, $state, Api) {
        var _this = this,
            modal = $modal.open({
                templateUrl: 'app/modules/admin/superlogin.html',
                controller: 'SuperLoginCtrl',
                controllerAs: 'ctrl'
            });

        modal.result.then(function(response) {
            if (!response) {
                $state.go('start');
            }
            _this.logged = true;
        });
        this.logged = false;
        this.Admin = Api.Admin;
    }
    SuperAdminCtrl.$inject = ['$modal', '$state', 'Api'];

    SuperAdminCtrl.prototype.reg = function (username, password) {
        this.Admin.save({
            username: username,
            password: md5(password)
        });
    };

    ng.module('app').controller('SuperAdminCtrl', SuperAdminCtrl);
}(window.angular, window.md5));