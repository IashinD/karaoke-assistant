/**
 * Created by iashind on 08.04.15.
 */

(function (ng) {

    ng.module('app',
        [
            'ngAnimate',
            'ui.bootstrap',
            'ui.bootstrap.tpls',
            'ngResource',
            'ui.sortable',
            'ui.router'
        ]);
}(window.angular));