/**
 * Created by iashind on 08.04.15.
 */
var express      = require('express'),
    path         = require('path'),
    logger       = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser   = require('body-parser'),
    session      = require('express-session'),
    MongoStore   = require('connect-mongo')(session),
    passport     = require('passport'),
    app          =  express(),
    boot         = require('./app/boot');

module.exports = app;

app.set('config', boot.config); // Create index.js with ENV support
app.services = boot.services;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    secret: 'ntAEAj',
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({ mongooseConnection: boot.services.db.connection })
}));
app.use(session({secret: 'ntAEAj', resave: false, saveUninitialized: false}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.services.router);

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (id, done) {
    done(null, id);
});

passport.use(boot.services.auth.LocalStrategy);

app.listen(app.get('port'), function () {
    console.log("App is running on port:" + app.get('port'));
    app.services.logger.info('-----------------------');
    app.services.logger.info('-----------------------');
    app.services.logger.info('App is running on port: ' + app.get('port'));
});

app.use(function (req, res) {
    return res.render('index');
});
