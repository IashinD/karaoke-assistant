var md5 = require('MD5'),
    boot = require('./../app/boot'),
    Admin = boot.services.db.model('Admin'),
    argv = process.argv[2];

function addManager() {
    var name = Math.ceil(Math.random() * 10e5),
        admin = new Admin({username: name, password: ''});

    admin.save(function (err) {
        if (err) {
            return console.log(err, admin);
        }
        console.log({username: admin.username});
    });
}

function removeManager(name) {
    Admin.findOne({username: name}).exec(function (err, admin) {
        if (err) {
            console.log(err);
        }
        admin.remove();
    });
}

function showManagers() {
    Admin.find({}).exec(function (err, admins) {
        var i;
        if (err) {
            return console.log(err);
        }
        for (i = 0; i < admins.length; i++) {
            console.log(i, admins[i]);
        }
    });
}

if (argv === '--add' || argv === '-a') {
    addManager();
} else if (argv === '--remove' || argv === '-r') {
    removeManager(process.argv[3]);
} else if (argv === '--show' || argv === '-s') {
    showManagers();
}
